# Project goals:

* Free and Open Source implementation of Virtual-Reality vision therapy suites like *Vivid Vision* and *Optics Trainer*.
* Implements vergence+torsion correction and anti-supression techniques: [Dichoptic Contrast Enhancement](https://www.cl.cam.ac.uk/research/rainbow/projects/dice/), [Monocular Fixation in Binocular Field](https://onlinelibrary.wiley.com/doi/full/10.1111/j.1444-0938.2010.00486.x), and [Interactive Binocular Treatment](https://www.nature.com/articles/6701883).
* It will implement a progress tracking system with anonymized data reporting, to be used for monitoring treatment progress in clinical setups, and for statistical analysis.
* Portable to any VR software/hardware platform. Based on the [Godot OpenXR](https://github.com/GodotVR/godot_openxr) game engine, it is compatible with standard VR headsets as well as smartphone-based low-cost systems like *Google Cardboard*.

![](images/screenshot-explained.png)
